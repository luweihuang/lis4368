
# lis 4368

## Lu Wei Huang

### Project 1 Requirements:


#### README.md file should include the following items:

1. Provide Bitbucket read-only access to p1 repo, *must* include README.md, using Markdown
  syntax.

2. Blackboard Links: p1 Bitbucket repo

README.md file should include the following items:

1. Course title, your name, assignment requirements, as per A1;

2. Screenshot of validation;

> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git rm - Remove files from the working tree and from the index

#### Assignment Deliverables:

*LIS4368 Portal (Main/Splash Page)*:

![LIS4368 Portal (Main/Splash Page)](img/one.png)

*Failed Validation*:

![Failed Validation](img/two.png)

*Passed Validation*:

![Passed Validation](img/three.png)










