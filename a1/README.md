> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 Advanced Web Applications Development 

## Lu Wei Huang

### Assignment1 # Requirements:

*Three Parts*

1. Distributed Version Control with Git and Bitbucket
2. Java/JSP/Sevlet 
3. Chapter Questions (Chs 1, 2)

#### README.md file should include the following items:

1. Screenshot of running java Hello (#1 above)

2. Screen Shot of http//localhost:999

3. git commands with short descriptions

4. Bitbucket repo links a) this assignment b) the completed tutorial above

> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git rm - Remove files from the working tree and from the index

#### Assignment Screenshots:

*Screenshot of JDK*:

![AMPPS Installation Screenshot](img/jdk_install.png)

*Screenshot of tomcat*:

![tomcat Screenshot](img/tomcat.png)

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations](https://bitbucket.org/luweihuang/bitbucketstationlocations/overview"Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes](https://bitbucket.org/luweihuang/myteamquotes "My Team Quotes Tutorial")




