LIS 4368

Lu Wei Huang

### Assignment # Requirements:

[A1 Subdirectory](a1/README.md "A1 Coursework")

1. Screenshot of running java Hello

2. Screen Shot of http//localhost:999

3. git commands with short descriptions

4. Bitbucket repo links a) this assignment b) the completed tutorial above

5. Chapter Questions

[A2 Subdirectory](a2/README.md "A2 Coursework")

1.  Download and install MySQL

2.  Finish tutorials that helps us setup the website

3.  The links should properly display (see screenshots below)

4.  Chapter Questions

[A3 Subdirectory](a3/README.md "A3 Coursework")

1. Create database 

2. Create ERD screenshot

3. Forward Engineer and save link to be attached

4. Complete sql statements

[A4 Subdirectory](a4/README.md "A4 Coursework")

1. cd to webapps subdirectory:
Example (windows): cd C:/tomcat/webapps

2. Clone assignment starter files:
git clone https://bitbucket.org/mjowett/lis4368_student_files

3. Follow instructions on the video provided

4. Test the inputs, take screenshot of inputs


[A5 Subdirectory](a5/README.md "A5 Coursework")

1. cd to webapps subdirectory:
Example (windows): cd C:/tomcat/webapps

2. Clone assignment starter files:
git clone https://bitbucket.org/mjowett/lis4368_student_files

3. Follow instructions on the videos provided

4. Test the inputs, take screenshot of inputs

[P1 Subdirectory](p1/README.md "P1 Coursework")

1. Clone Assignment to starter files

2. Open Index.jsp and review code

3. Check the validation on each of the sections

4. Follow instructions for form validation

[P2 Subdirectory](p2/README.md "P2 Coursework")

1. Clone Assignment to starter files

2. Review Videos

3. Make necessary adjustments

4. Follow instructions for form validation

