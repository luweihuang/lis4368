> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 Advanced Web Applications Development 

## Lu Wei Huang

### Assignment 3 Requirements:

1. Create database 

2. Create ERD screenshot

3. Forward Engineer and save link to be attached

4. Complete sql statements

README.md file should include the following items:

1. Course title, your name, assignment requirements, as per A1;

2. Screenshot of ERD;

3. Links to the following files:
	a. a3.mwb
	b. a3.sql

> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git rm - Remove files from the working tree and from the index

#### Assignment Screenshots:

*ERD*:

![ERD](img/first.png)

*Link to a3.sql*:

[Link to a3.sql](a3.sql)

*Link to a3.mwb*:

[Link to a3.mwb](a3.mwb)



