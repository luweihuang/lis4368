> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 Advanced Web Applications Development 

## Lu Wei Huang

### Assignment 5 Requirements:

1. cd to webapps subdirectory:
Example (windows): cd C:/tomcat/webapps

2. Clone assignment starter files:
git clone https://bitbucket.org/mjowett/lis4368_student_files
(clones lis4368_student_files Bitbucket repo)
Note: If there is an existing lis4368_student_files, it *will* need to be renamed—otherwise, error!
a. Review subdirectories and files, especially META-INF and WEB-INF
b. Each assignment *must* have its own global subdirectory.

3. NB: Because of the way the newer version of Tomcat recognizes web applications (as well as
associated include files), *all* of the assignments will need to be placed under lis4368.

README.md file should include the following items:

1. Course title, your name, assignment requirements, as per A1;

2. Screenshot of failed input;

3. Screenshot of passed input;


> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git rm - Remove files from the working tree and from the index

#### Assignment Screenshots:

*Valid User Form Entry*:

![Entry](img/first.png)

*Passed Validation*:

![Entry](img/second.png)

*Associated Database Entry*:

![Entry](img/third.png)




