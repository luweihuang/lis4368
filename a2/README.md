> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 Advanced Web Applications Development 

## Lu Wei Huang

### Assignment2 Requirements:

1. Download and install MySQL:

2. Finish the following tutorial: https://www.ntu.edu.sg/home/ehchua/programming/howto/Tomcat_HowTo.html 2.5 STEP 5: Develop and Deploy a WebApp to… 2.8 (Advanced) Deploying Servlet using @WebServlet (Servlet 3.0 on Tomcat 7), inclusive. (Note: we will be using the @WebServlet method for the remaining assignments.)

3. The links should properly display (see screenshots below):

#### README.md file should include the following items:

1. Assessment links (as above), and

2. Screenshot of the query results from the following link (see screenshot below): http://localhost:9999/hello/querybook.html

> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git rm - Remove files from the working tree and from the index

#### Assignment Screenshots:

*http://localhost:9999/hello*:

![http://localhost:9999/hello](img/one.png)

*http://localhost:9999/hello/index.html*:

![tomcat Screenshot](img/two.png)

*http://localhost:9999/hello/sayhello*:

![http://localhost:9999/hello](img/three.png)

*http://localhost:9999/hello/querybook.html*:

![tomcat Screenshot](img/four.png)

*http://localhost:9999/hello/sayhi*:

![http://localhost:9999/hello](img/five.png)

*http://localhost:9999/hello/querybook.html*:

![tomcat Screenshot](img/six.png)

*query results*:

![http://localhost:9999/hello](img/seven.png)


